package com.dicom.utils;
import ij.plugin.DICOM;
import ij.util.DicomTools;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.hibernate.type.descriptor.java.UUIDTypeDescriptor;

import com.dicom.entity.Pic;
import com.dicom.entity.Sick;

/**
 * <b></b>
 * <b>创建时间</b> 2015/5/13
 * <b>最后更新时间</b> 2015/5/13
 *
 * @version 1.0
 * @auther
 */
public class ParseDicomtFileUtils {

	public static File imgFileDir = null;
    public static List<DICOM> parseDir(File dicomDir) {
        List<DICOM> dicoms = new ArrayList<DICOM>();
        if (dicomDir == null || !dicomDir.exists()) return null;
        for (File file : dicomDir.listFiles()) {
            if (file.isDirectory()) {
                dicoms.addAll(parseDir(file));
            }
            if (".dcm".equalsIgnoreCase(getExpandStr(file.getName()))){
                dicoms.add(parseFile(file));
            }
        }
        return dicoms;
    }
    
    public static List<Sick> getPatients(List<DICOM> dicoms,HttpServletRequest request) {
    	List<Sick> sicks = new ArrayList<Sick>();
    	for (DICOM dicom : dicoms) {
    		sicks.add(getPatient(dicom,request));
    	}
    	return sicks;
    }

    public static Sick getPatient(DICOM dicom,HttpServletRequest request) {
    	imgFileDir=new File(request.getSession().getServletContext().getRealPath("\\resources\\front\\image"));
        Sick sick = new Sick();
        sick.setName(DicomTools.getTag(dicom, "Patient's Name"));
        sick.setId(DicomTools.getTag(dicom, "Patient ID").trim());
        sick.setSex(DicomTools.getTag(dicom, "Patient's Sex"));
        String ageStr = DicomTools.getTag(dicom, "Patient's Age");
        sick.setAge(ageStr);
        String uuid=UUID.randomUUID().toString().replaceAll("-", "");
        Pic pic = new Pic();
        pic.setSick(sick);
        pic.setId(uuid);
        pic.setUrl(uuid + ".jpg");
        sick.addPic(pic);
        createImageFile(dicom,uuid);
        return sick;
    }
    public static DICOM parseFile(File dicomFile) {
        DICOM dicom = new DICOM();
        dicom.run(dicomFile.getAbsolutePath());
        return dicom;
    }

    /**
     * 根据Dicom文件生成图片
     * @param dicomFile
     * @return
     */
    public static File createImageFile(File dicomFile,HttpServletRequest request) {
    	imgFileDir=new File(request.getSession().getServletContext().getRealPath("\\resources\\front\\image"));
        DICOM dicom = parseFile(dicomFile);
        return createImageFile(dicom,UUID.randomUUID().toString());
    }

    public static File createImageFile(DICOM dicom,String uuid) {
    	 if (!imgFileDir.exists()) imgFileDir.mkdirs();
    	 File imgFile = new File(imgFileDir, uuid+".jpg");
         if (imgFile.exists()) imgFile.delete();
         try {
             ImageIO.write(dicom.getBufferedImage(), "jpg", imgFile);
         } catch (IOException e) {
             e.printStackTrace();
         }
         return imgFile;
    }

    /**
     * 获取后缀名
     * @return
     */
    public static String getExpandStr(String fileName) {
        return fileName.substring(fileName.lastIndexOf('.'), fileName.length());
    }

}
