package com.dicom.dao;

import java.util.Set;

import com.dicom.entity.Pic;

public interface PicDao {

	public Pic get(String id);
	
	public void save(Pic pic);
	
	public void saveAll(Set<Pic> pics);
}
