package com.dicom.dao;

import java.util.List;

import com.dicom.common.Page;
import com.dicom.entity.Sick;

public interface SickDao {

	public Page getList(Page page);
	
	public Sick get(String id);
	
	public void save(Sick sick);
	
	public void saveAll(List<Sick> sicks);
	
}
