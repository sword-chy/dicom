package com.dicom.dao.impl;

import java.util.Set;

import org.springframework.stereotype.Repository;

import com.dicom.common.HibernateSimpleDao;
import com.dicom.dao.PicDao;
import com.dicom.entity.Pic;

@Repository
public class PicDaoImpl extends HibernateSimpleDao implements PicDao{

	public Pic get(String id){
		return (Pic) getSession().get(Pic.class, id);
	}
	
	public void save(Pic pic) {
		Pic oldPic = get(pic.getId());
		if (oldPic == null) {
			getSession().save(pic);
		}
	}
	
	public void saveAll(Set<Pic> pics) {
		if (pics == null) return;
		for (Pic pic : pics) {
			save(pic);
		}
		getSession().flush();
	}
}
