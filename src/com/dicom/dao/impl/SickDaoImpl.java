package com.dicom.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dicom.common.Finder;
import com.dicom.common.HibernateSimpleDao;
import com.dicom.common.Page;
import com.dicom.dao.PicDao;
import com.dicom.dao.SickDao;
import com.dicom.entity.Sick;

@Repository
public class SickDaoImpl extends HibernateSimpleDao implements SickDao {

	public Page getList(Page page) {
		String sql = "from Sick";
		Finder f = Finder.create(sql);
		return find(f, page.getPageNo(), page.getPageSize());
	}

	public Sick get(String id) {
		return (Sick) getSession().get(Sick.class, id);
	}

	public void save(Sick sick) {
		if (sick == null)
			return;
		Sick oldSick = get(sick.getId());
		if (oldSick == null) {
			getSession().save(sick);
		} 
		picDao.saveAll(sick.getList());
	}

	public void saveAll(List<Sick> sicks) {
		if (sicks == null) {
			return;
		}
		for (Sick sick : sicks) {
			save(sick);
		}
		getSession().flush();
	}

	@Autowired
	private PicDao picDao;
}
