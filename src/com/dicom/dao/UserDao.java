package com.dicom.dao;

import java.util.List;

import com.dicom.common.Page;
import com.dicom.entity.User;

public interface UserDao {

	public User getById(Long id);
	
	public List<User> getList();
	
	public Long save(User user);
	
	public User getByUsername(String username);
	
	public User getByUsernameAndPassword(User user);
	
	public User update(User user);
	
	public void delete(Long userId);
	
	public Page getFreeEmployer(Page page);
	
	public Page getEmployee(Page page);
}
