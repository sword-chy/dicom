package com.dicom.controller.front;
import ij.plugin.DICOM;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dicom.dao.SickDao;
import com.dicom.entity.Sick;
import com.dicom.utils.ParseDicomtFileUtils;
import com.dicom.utils.ResponseUtils;

/**
 * <b></b>
 * <b>创建时间</b> 2015/5/9
 * <b>最后更新时间</b> 2015/5/9
 *
 * @version 1.0
 * @auther
 */
@Controller
public class Test {
	
	@RequestMapping(value="savesick")
    public void test(HttpServletResponse response,HttpServletRequest request) {
        List<DICOM> dicomList = ParseDicomtFileUtils.parseDir(new File("d:/dicom/SRS00001"));
        List<Sick> sicks = ParseDicomtFileUtils.getPatients(dicomList,request);
        sickDao.saveAll(sicks);
        ResponseUtils.renderJson(response, "执行成功");
    }
    
    @Autowired
    private SickDao sickDao;

}
