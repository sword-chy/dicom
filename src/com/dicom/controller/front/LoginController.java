package com.dicom.controller.front;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dicom.entity.User;
import com.dicom.manager.UserMng;

@Controller
public class LoginController {

	@RequestMapping(value="login",method=RequestMethod.GET)
	public String getPage(){
		return "login";
	}
	
	@RequestMapping(value="login",method=RequestMethod.POST)
	public String login(User user,ModelMap model,HttpServletRequest request){
		Long sessionUserId=(Long) request.getSession().getAttribute("userId");
		User hasUser=null;
		if(sessionUserId==null){
			hasUser=userMng.login(user);
			if(hasUser!=null){
				request.getSession().setAttribute("userId", hasUser.getId());
			}
		}
		if(hasUser!=null){
			return "redirect:/sick/list";
		}else{
			model.put("res", "登录失败");
			return getPage();
		}
	}
	
	@RequestMapping(value="logout")
	public String logout(HttpServletRequest request){
		request.getSession().removeAttribute("userId");
		return "redirect:/login";
	}
	
	@Autowired
	private UserMng userMng;
}
