package com.dicom.entity;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Sick {

	private String id;
	private String name;
	private String sex;
	private String age;
	private Set<Pic> list;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public Set<Pic> getList() {
		return list;
	}
	public void setList(Set<Pic> list) {
		this.list = list;
	}
	
	public void addPic(Pic pic) {
		if (list == null) {
			list = new HashSet<Pic>();
		}
		list.add(pic);
	}
	
	public void addPics(Set<Pic> pics ) {
		if (list == null) {
			list = new HashSet<Pic>();
		}
		list.addAll(pics);
	}
}
