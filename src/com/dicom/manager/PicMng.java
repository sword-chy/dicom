package com.dicom.manager;

import java.util.List;

import com.dicom.entity.Pic;

public interface PicMng {

	public List<Pic> getList(String[] ids);
}
