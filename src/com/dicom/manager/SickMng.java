package com.dicom.manager;

import com.dicom.common.Page;
import com.dicom.entity.Sick;

public interface SickMng {

	public Page getList(Page page);
	
	public Sick getSick(String id);
}
