package com.dicom.manager.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dicom.dao.PicDao;
import com.dicom.entity.Pic;
import com.dicom.manager.PicMng;

@Service
public class PicMngImpl implements PicMng{

	public List<Pic> getList(String[] ids){
		List<Pic> list=new ArrayList<Pic>();
		for(int i=0;i<ids.length;i++){
			Pic pic=dao.get(ids[i]);
			list.add(pic);
		}
		return list;
	}
	@Autowired
	private PicDao dao;
}
