package com.dicom.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dicom.common.Page;
import com.dicom.dao.SickDao;
import com.dicom.entity.Sick;
import com.dicom.manager.SickMng;

@Service
public class SickMngImpl implements SickMng{

	public Page getList(Page page){
		return dao.getList(page);
	}
	
	public Sick getSick(String id){
		return dao.get(id);
	}
	
	@Autowired
	private SickDao dao;
}
