package com.dicom.manager;

import java.util.List;

import com.dicom.common.Page;
import com.dicom.entity.User;

public interface UserMng {

	public User getById(Long id);
	
	public List<User> getList();
	
	public boolean register(User user);
	
	public User getByUsername(String username);
	
	public User login(User user);
	
	public User update(User user);
	
	public Page getFreeEmployer(Page page);
	
	public Page getEmployee(Page page);
}
